package com.markalanricahrds.demo.demobackend.featureflags.model

data class FeatureFlag(val name: String)
