mkdir -p ~/.aws
cat <<EOF >  ~/.aws/credentials
[ciuser]
aws_access_key_id = ${CI_AWS_ACCESS_KEY_ID}
aws_secret_access_key = ${CI_AWS_SECRET_ACCESS_KEY}
EOF

cat <<EOF > ~/.aws/config
[default]
region = ${AWS_DEFAULT_REGION}

[profile ciuser]

[profile ${AWS_PROFILE}]
role_arn = ${AWS_PROFILE_ROLE_ARN}
source_profile = ciuser
region = ${AWS_DEFAULT_REGION}
EOF