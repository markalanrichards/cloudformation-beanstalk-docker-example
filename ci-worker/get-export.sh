#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi
aws cloudformation list-exports --output  json  2> /dev/null  | jq '.Exports | map(select(.Name=="'${1}'")) | .[0].Value' -r
