package com.markalanricahrds.demo.demobackend.featureflags.table

import com.markalanricahrds.demo.demobackend.featureflags.table.FeatureFlagsTable.FeatureFlagRecord
import org.jooq.Table
import org.jooq.TableField
import org.jooq.impl.CustomRecord
import org.jooq.impl.CustomTable
import org.jooq.impl.DSL
import org.jooq.impl.SQLDataType

class FeatureFlagsTable private constructor() : CustomTable<FeatureFlagRecord>(DSL.name("feature_flags")) {
    companion object {
        val featureFlagsTable = FeatureFlagsTable()
        val featureName: TableField<FeatureFlagRecord, String>? =
            featureFlagsTable.createField(DSL.name("feature_name"), SQLDataType.VARCHAR.identity(true).notNull())
    }

    class FeatureFlagRecord(table: Table<FeatureFlagRecord>?) : CustomRecord<FeatureFlagRecord>(table)

    override fun getRecordType(): Class<FeatureFlagRecord> {
        return FeatureFlagRecord::class.java
    }

}