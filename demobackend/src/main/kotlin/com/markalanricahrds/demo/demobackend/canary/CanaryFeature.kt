package com.markalanricahrds.demo.demobackend.canary

import reactor.core.publisher.Mono
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class CanaryFeature {
    fun canary(): Mono<CanaryEntity> {
        return Mono.just(CanaryEntity(ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_ZONED_DATE_TIME)))
    }
}