package com.markalanricahrds.demo.demobackend.api

import com.markalanricahrds.demo.demobackend.canary.CanaryEntity
import com.markalanricahrds.demo.demobackend.canary.CanaryFeature
import com.markalanricahrds.demo.demobackend.flaggedcanary.FlaggedCanaryEntity
import com.markalanricahrds.demo.demobackend.featureflags.FeatureFlagsFeature
import com.markalanricahrds.demo.demobackend.featureflags.model.FeatureFlag
import io.micrometer.core.annotation.Timed
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api")
@Timed(histogram = true, percentiles = [0.05, 0.95])
class ApiController(val canaryFeature: CanaryFeature, val featureFlagsFeature: FeatureFlagsFeature) {
    @GetMapping("/canary")
    fun canary(): Mono<CanaryEntity> {
        return canaryFeature.canary()
    }

    @GetMapping("/flagged-canary")
    fun flaggedCanary(): Mono<FlaggedCanaryEntity> {
        return featureFlagsFeature.flaggedCanary().flatMap { it.flaggedCanary() }
    }
    @PostMapping("/features/swap/{flag}")
    fun enableFlaggedCanary(@PathVariable flag: String) {
        featureFlagsFeature.swap(flag)
    }
    @GetMapping("/features")
    fun featureFlags(): Mono<Set<FeatureFlag>> {
        return featureFlagsFeature.features()
    }

}