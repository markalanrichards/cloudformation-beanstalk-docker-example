package com.markalanricahrds.demo.demobackend

import com.markalanricahrds.demo.demobackend.featureflags.DslContextSupplier
import com.markalanricahrds.demo.demobackend.featureflags.KMysqlContainer
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ActiveProfiles
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers


@SpringBootTest
@Testcontainers
@ActiveProfiles("test")
class DemobackendApplicationTests {

    @MockBean
    lateinit var dslContextSupplier: DslContextSupplier

    @Container
    private val mysqlContainer = KMysqlContainer()



    @Test
    fun contextLoads() {
    }

}
