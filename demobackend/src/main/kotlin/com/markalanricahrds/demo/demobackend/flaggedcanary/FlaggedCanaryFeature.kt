package com.markalanricahrds.demo.demobackend.flaggedcanary

import reactor.core.publisher.Mono
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class FlaggedCanaryFeature {
    fun flaggedCanary(): Mono<FlaggedCanaryEntity> {
        return Mono.just(
            FlaggedCanaryEntity(
                ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_ZONED_DATE_TIME)
            )
        )
    }
}