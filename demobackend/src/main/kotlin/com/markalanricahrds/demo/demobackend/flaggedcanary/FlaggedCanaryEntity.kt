package com.markalanricahrds.demo.demobackend.flaggedcanary

data class FlaggedCanaryEntity(val datetime: String)