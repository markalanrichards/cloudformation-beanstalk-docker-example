package com.markalanricahrds.demo.demobackend


import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.markalanricahrds.demo.demobackend.canary.CanaryFeature
import com.markalanricahrds.demo.demobackend.featureflags.DslContextSupplier
import com.markalanricahrds.demo.demobackend.featureflags.FeatureFlagsFeature
import com.markalanricahrds.demo.demobackend.flaggedcanary.FlaggedCanaryFeature
import org.jooq.DSLContext
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Profile
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest
import java.util.function.Supplier


@Lazy
@SpringBootApplication(exclude = [R2dbcAutoConfiguration::class])
class DemobackendApplication {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Bean
    @Profile("!test")
    fun dslContextSupplier(
        @Value("\${aurora.secret}") auroraLoginSecret: String,
        @Value("\${aurora.endpoint}") auroraEndpoint: String
    ): Supplier<DSLContext> {
        try {
            val secret: String = SecretsManagerClient.builder()
                .build().use { secretsClient ->
                    getValue(secretsClient, auroraLoginSecret);
                }
            val mapper = jacksonObjectMapper()
            val typeReference = object : TypeReference<Map<String, String>>() {}
            val secretMap: Map<String, String> = mapper.readValue(secret, typeReference)
            return DslContextSupplier(auroraEndpoint, secretMap.get("password")!!, secretMap.get("username")!!)
        } catch (e: Exception) {
            logger.error("Cannot create a database supplier", e)
            return Supplier { throw RuntimeException("Missing implementation") }
        }
    }

    fun getValue(secretsClient: SecretsManagerClient, secretName: String?): String {
        return secretsClient.getSecretValue(
            GetSecretValueRequest.builder()
                .secretId(secretName)
                .build()
        ).secretString()


    }

    @Bean
    fun featureFlagsFeature(
        dslContextSupplier: Supplier<DSLContext>,
        flaggedCanaryFeature: FlaggedCanaryFeature
    ): FeatureFlagsFeature {
        return FeatureFlagsFeature(dslContextSupplier, flaggedCanaryFeature)
    }

    @Bean
    fun canaryFeature(): CanaryFeature {
        return CanaryFeature()
    }

    @Bean
    fun flaggedCanaryFeature(): FlaggedCanaryFeature {
        return FlaggedCanaryFeature()
    }
}

fun main(args: Array<String>) {
    runApplication<DemobackendApplication>(*args)

}
