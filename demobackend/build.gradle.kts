import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
	`maven-publish`
	id("com.github.ben-manes.versions") version "0.39.0"
}

group = "com.markalanricahrds.demo"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_16

repositories {
	mavenCentral()
}
val publishRepo = findProperty("publishRepo") ?: "https://fail.invalid"
publishing {
	publications {
		create<MavenPublication>("mavenJava") {

		}
	}
	repositories {
		maven {
			url = uri(publishRepo)
		}
	}
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jooq:jooq:3.15.1")
	implementation("org.jooq:jooq-meta:3.15.1")
	implementation("com.zaxxer:HikariCP:5.0.0")
	testImplementation("org.testcontainers:mysql:1.16.0")
	testImplementation("org.testcontainers:junit-jupiter:1.16.0")
	testImplementation("mysql:mysql-connector-java:8.0.26")
	implementation("org.mariadb.jdbc:mariadb-java-client:2.7.4")

	implementation("com.github.ben-manes.caffeine:caffeine:3.0.3")
	implementation("software.amazon.awssdk:secretsmanager:2.17.21")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "16"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}