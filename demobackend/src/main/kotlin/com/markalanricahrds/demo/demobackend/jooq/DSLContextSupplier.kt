package com.markalanricahrds.demo.demobackend.featureflags

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import java.util.function.Supplier

open class DslContextSupplier(url: String,
                         password: String,
                         user: String) : Supplier<DSLContext>, AutoCloseable {
    private val dslContext: DSLContext
    private val hikariDataSource: HikariDataSource
    override fun get(): DSLContext {
        return dslContext
    }

    override fun close() {
        hikariDataSource.close()
    }

    init {
        val configuration = HikariConfig()
        configuration.jdbcUrl = url
        configuration.username = user
        configuration.password = password
        configuration.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        configuration.isAutoCommit = false
        configuration.isReadOnly = false

        hikariDataSource = HikariDataSource(configuration)
        try {
            dslContext = DSL.using(hikariDataSource, SQLDialect.MYSQL)
        } catch (e: RuntimeException) {
            hikariDataSource.close()
            throw e
        }
    }
}
