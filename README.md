# aws-sandbox

You likely need to run the `cloudformation/ci-user/ci-user.yml` with a root account to start with.

Then you'll need to manually create a key pair for the CI_USER and then fill out the following.

These gitlab CI/CD variables are needed.
You can pick the stack names in advance.
* AWS_DEFAULT_REGION: eg eu-west-2
* AWS_PROFILE: eg: myprofile
* CI_AWS_ACCESS_KEY_ID: generate a IAM security token manually for the CI User
* CI_AWS_SECRET_ACCESS_KEY: generate a IAM security token manually for the CI User
* CI_USER_STACK_NAME: eg: ciuser
* EBS_NGINX_STACK_NAME: eg: ebs-nginx
* VPC_STACK_NAME: eg: vpc