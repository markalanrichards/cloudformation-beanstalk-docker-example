CREATE TABLE feature_flags
(
    feature_name  VARCHAR(256) UNIQUE NOT NULL,
    PRIMARY KEY (feature_name)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8mb4;