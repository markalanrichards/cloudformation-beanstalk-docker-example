package com.markalanricahrds.demo.demobackend.featureflags

import com.github.benmanes.caffeine.cache.Caffeine
import com.markalanricahrds.demo.demobackend.flaggedcanary.FlaggedCanaryFeature
import com.markalanricahrds.demo.demobackend.featureflags.model.FeatureFlag
import org.jooq.DSLContext
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.TimeUnit
import java.util.function.Supplier


class FeatureFlagsFeature(
    val dslContextSupplier: Supplier<DSLContext>,
    val flaggedCanaryFeature: FlaggedCanaryFeature
) {
    val flaggedCanary = "flagged_canary"
    val knownFlags = setOf(flaggedCanary)
    val featureFlagsRepository = FeatureFlagsRepository()

    val featureFlagCachedByKey = Caffeine
        .newBuilder()
        .expireAfterWrite(1, TimeUnit.MINUTES)
        .buildAsync<String, Boolean> { key ->
            dslContextSupplier
                .get()
                .transactionResult { configuration ->
                    featureFlagsRepository.featureExists(
                        configuration,
                        FeatureFlag(key)
                    )
                }

        }
    val featureFlagWriteCacheByKey = Caffeine
        .newBuilder()
        .expireAfterWrite(1, TimeUnit.MINUTES)
        .buildAsync<String, Boolean> { key ->
            dslContextSupplier
                .get()
                .transactionResult { configuration ->
                    val exists = featureFlagsRepository.featureExists(
                        configuration,
                        FeatureFlag(key)
                    )
                    if (exists) featureFlagsRepository.delete(
                        configuration,
                        FeatureFlag(key)
                    ) else featureFlagsRepository.save(
                        configuration,
                        FeatureFlag(key)
                    )
                }

        }


    val allFeatureFlagsCached = Caffeine
        .newBuilder()
        .expireAfterWrite(1, TimeUnit.MINUTES)
        .buildAsync<String, Set<FeatureFlag>> { key ->
            dslContextSupplier
                .get()
                .transactionResult { configuration ->
                    featureFlagsRepository.features(
                        configuration
                    )
                }
        }


    fun flaggedCanary(): Mono<FlaggedCanaryFeature> {
        return featureFlagCachedByKey.get(flaggedCanary).toMono().flatMap { exists ->
            if (exists!!) Mono.just(flaggedCanaryFeature) else Mono.empty()
        }

    }

    fun features(): Mono<Set<FeatureFlag>> {
        return allFeatureFlagsCached.get("").toMono()
    }


    fun swap(flag: String) {
        if (knownFlags.contains(flag)) featureFlagWriteCacheByKey.get(flag)
    }
}