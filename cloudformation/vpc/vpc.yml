AWSTemplateFormatVersion: 2010-09-09
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: "10.0.0.0/16"
      EnableDnsSupport: true
      EnableDnsHostnames: true

  InternetGateway:
    Type: AWS::EC2::InternetGateway
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway

  #
  # Routing - public subnets
  #
  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
  PublicRoute:
    Type: AWS::EC2::Route
    DependsOn: AttachGateway
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  PublicSubnet0:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      Tags:
        - Key: SubnetGroup
          Value: Public
      CidrBlock: !Select [ 4, !Cidr [ !GetAtt VPC.CidrBlock, 8, 6 ] ]
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]

  PublicSubnetRouteTableAssociation0:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnet0
      RouteTableId: !Ref PublicRouteTable

  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      Tags:
        - Key: SubnetGroup
          Value: Public
      CidrBlock: !Select [ 5, !Cidr [ !GetAtt VPC.CidrBlock, 8, 6 ] ]
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs '' ]

  PublicSubnetRouteTableAssociation1:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnet1
      RouteTableId: !Ref PublicRouteTable

  #
  # Routing - private subnets
  #
  NatGatewayAttachment0:
    Type: AWS::EC2::EIP
    DependsOn: AttachGateway
    Properties:
      Domain: vpc

  NatGateway0:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGatewayAttachment0.AllocationId
      SubnetId: !Ref PublicSubnet0

  PrivateRouteTable0:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC

  PrivateRoute0:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable0
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway0

  PrivateSubnet0:
    Type: AWS::EC2::Subnet
    Properties:
      Tags:
        - Key: SubnetGroup
          Value: Private
      MapPublicIpOnLaunch: false
      CidrBlock: !Select [ 0, !Cidr [ !GetAtt VPC.CidrBlock, 8, 6 ] ]
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]

  PrivateSubnetRouteTableAssociation0:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PrivateSubnet0
      RouteTableId: !Ref PrivateRouteTable0

  NatGatewayAttachment1:
    Type: AWS::EC2::EIP
    DependsOn: AttachGateway
    Properties:
      Domain: vpc

  NatGateway1:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGatewayAttachment1.AllocationId
      SubnetId: !Ref PublicSubnet1

  PrivateRouteTable1:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC

  PrivateRoute1:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable1
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway1

  PrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      Tags:
        - Key: SubnetGroup
          Value: Private
      MapPublicIpOnLaunch: false
      CidrBlock: !Select [ 1, !Cidr [ !GetAtt VPC.CidrBlock, 8, 6 ] ]
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs '' ]

  PrivateSubnetRouteTableAssociation1:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PrivateSubnet1
      RouteTableId: !Ref PrivateRouteTable1

Outputs:
  VpcId:
    Value: !Ref VPC
    Export:
      Name: !Sub '${AWS::StackName}-VpcId'
  DefaultSecurityGroup:
    Value: !GetAtt VPC.DefaultSecurityGroup
    Export:
      Name: !Sub '${AWS::StackName}-VpcDefaultSecurityGroup'
  PrivateSubnets:
    Value: !Join [ ",", [ !Ref PrivateSubnet0 , !Ref PrivateSubnet1 ] ]
    Export:
      Name: !Sub '${AWS::StackName}-PrivateSubnets'
  PublicSubnets:
    Value: !Join [ ",", [ !Ref PublicSubnet0 , !Ref PublicSubnet1 ] ]
    Export:
      Name: !Sub '${AWS::StackName}-PublicSubnets'
 