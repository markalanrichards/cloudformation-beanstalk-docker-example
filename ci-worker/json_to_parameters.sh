#!/usr/bin/env bash
jq 'to_entries | [ .[] | {"ParameterKey":.key, "ParameterValue": .value}]' < /dev/stdin