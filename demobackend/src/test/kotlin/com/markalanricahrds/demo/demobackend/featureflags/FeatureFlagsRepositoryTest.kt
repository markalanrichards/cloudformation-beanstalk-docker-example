package com.markalanricahrds.demo.demobackend.featureflags

import com.markalanricahrds.demo.demobackend.featureflags.model.FeatureFlag
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import java.sql.DriverManager

class KMysqlContainer : MySQLContainer<KMysqlContainer>("mysql:5.7")

@Testcontainers
internal class FeatureFlagsRepositoryTest {
    @Container
    private val mysqlContainer = KMysqlContainer()


    @Test
    fun testContainerRuns() {
        assertTrue(mysqlContainer.isRunning());
    }

    @Test
    fun testCreateFeatureFlagsTable() {
        createFeatureFlagsTable()
    }

    @Test
    fun testSaveFeature() {
        createFeatureFlagsTable()
        val featureFlagsRepository = FeatureFlagsRepository()
        val dslContext = DslContextSupplier(
            url = mysqlContainer.jdbcUrl,
            user = mysqlContainer.username,
            password = mysqlContainer.password
        ).get()!!
        val testingFlag = FeatureFlag("testing")
        dslContext.transactionResult { configuration ->
            featureFlagsRepository.save(
                configuration,
                testingFlag
            )
        }
        assertTrue(
            dslContext.transactionResult { configuration ->
                featureFlagsRepository.featureExists(
                    configuration,
                    testingFlag
                )
            })
    }

    @Test
    fun testSaveFeatureHasNoFalsePositive() {
        createFeatureFlagsTable()
        val featureFlagsRepository = FeatureFlagsRepository()
        val dslContext = DslContextSupplier(
            url = mysqlContainer.jdbcUrl,
            user = mysqlContainer.username,
            password = mysqlContainer.password
        ).get()!!
        val testingFlag = FeatureFlag("testing")
        val notTestingFlag = FeatureFlag("notTesting")
        dslContext.transactionResult { configuration ->
            featureFlagsRepository.save(
                configuration,
                testingFlag
            )
        }
        assertFalse(
            dslContext.transactionResult { configuration ->
                featureFlagsRepository.featureExists(
                    configuration,
                    notTestingFlag
                )
            })
    }

    @Test
    fun testFeatures() {
        createFeatureFlagsTable()
        val featureFlagsRepository = FeatureFlagsRepository()
        val dslContext = DslContextSupplier(
            url = mysqlContainer.jdbcUrl,
            user = mysqlContainer.username,
            password = mysqlContainer.password
        ).get()!!
        val testingFlag = FeatureFlag("testing")
        val anotherTestingFlag = FeatureFlag("anotherTesting")
        dslContext.transactionResult { configuration ->
            featureFlagsRepository.save(
                configuration,
                testingFlag
            )
        }
        dslContext.transactionResult { configuration ->
            featureFlagsRepository.save(
                configuration,
                anotherTestingFlag
            )
        }
        assertEquals(
            setOf(FeatureFlag("testing"), FeatureFlag("anotherTesting")),
            dslContext.transactionResult { configuration ->
                featureFlagsRepository.features(
                    configuration
                )
            }
        )

    }

    private fun createFeatureFlagsTable() {
        DriverManager.getConnection(mysqlContainer.jdbcUrl, mysqlContainer.username, mysqlContainer.password)
            .use { connection ->
                connection.createStatement().use { statement ->
                    statement.execute(
                        """
                                  CREATE TABLE feature_flags
                                  (
                                      feature_name  VARCHAR(256) UNIQUE NOT NULL,
                                      PRIMARY KEY (feature_name)
                                  ) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8mb4;
                              """.trimIndent()
                    )
                }
            }
    }

}