package com.markalanricahrds.demo.demobackend.featureflags

import com.markalanricahrds.demo.demobackend.featureflags.model.FeatureFlag
import com.markalanricahrds.demo.demobackend.featureflags.table.FeatureFlagsTable.Companion.featureFlagsTable
import com.markalanricahrds.demo.demobackend.featureflags.table.FeatureFlagsTable.Companion.featureName
import org.jooq.Configuration
import org.jooq.impl.DSL.selectOne
import org.jooq.impl.DSL.using

class FeatureFlagsRepository {
    fun save(configuration: Configuration, featureFlag: FeatureFlag): Boolean {
        return using(configuration).insertInto(featureFlagsTable)
            .set(featureName, featureFlag.name)
            .execute() > 0
    }
    fun delete(configuration: Configuration, featureFlag: FeatureFlag): Boolean {
        return using(configuration).deleteFrom(featureFlagsTable)
            .where(featureName?.eq(featureFlag.name))
            .execute() > 0
    }

    fun featureExists(configuration: Configuration, featureFlag: FeatureFlag): Boolean {
        return using(configuration)
            .fetchExists(
                selectOne()
                    .from(featureFlagsTable)
                    .where(featureName?.eq(featureFlag.name))
            )
    }

    fun features(configuration: Configuration): Set<FeatureFlag> {
        return using(configuration).select(featureName)
            .from(featureFlagsTable)
            .fetch()
            .map { FeatureFlag(it.get(featureName)) }
            .toSet()
    }

}